package com.example.matheus.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etNumber1;
    private EditText etNumber2;
    private RadioGroup rgFunction;
    private TextView tvResult;
    boolean error = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etNumber1 = (EditText) findViewById(R.id.etNumber1);
        etNumber2 = (EditText) findViewById(R.id.etNumber2);
        rgFunction = (RadioGroup) findViewById(R.id.rgFunction);
        tvResult = (TextView) findViewById(R.id.tvResult);
    }

    public void calcular(View view) {
        double number1, number2, result;
        number1 = validate(etNumber1);
        number2 = validate(etNumber2);
        result = 0;

        String function = ((RadioButton) findViewById(rgFunction.getCheckedRadioButtonId())).getText().toString();
        if(!error){
            result = calculate(function, number1, number2);
        }

        if(!error){
            tvResult.setText(getResources().getString(R.string.result, result));
        }

    }

    public double calculate(String action, double number1, double number2) {
        switch (action) {
            case "Sum":
                return number1 + number2;
            case "Substraction":
                return number1 - number2;
            case "Division":
                if (number2 == 0) {
                    impossibleDivision();
                }
                return number1 / number2;
            case "Multiplication":
                return number1 * number2;
            default:
                return 0;
        }
    }

    public void impossibleDivision(){
        etNumber2.setError(getResources().getString(R.string.division_by_zero));
        error = true;
    }

    public Double validate(EditText value){
        Double number = 0.0;
        try{
            number = Double.parseDouble(value.getText().toString());
            value.setError(null);
        } catch (Exception e) {
            value.setError(getResources().getString(R.string.conversion_failed));
            error = true;
        }
        return number;
    }
}
